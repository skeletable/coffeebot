#!/usr/bin/env coffee

Eris = require "eris"
YoutubeDlWrap = require "youtube-dl-wrap"
fs = require "node:fs"
Constants = Eris.Constants

bot = new Eris process.argv[2]
youtube = new YoutubeDlWrap("youtube-dl")

bot.on "ready", () ->
    console.log "READY"
    commands = await bot.getCommands
    if commands?
        bot.createCommand
            name: "play"
            description: "Play audio from a link."
            options: [
                "name": "link"
                "description": "Link to the audio"
                "type": Constants.ApplicationCommandOptionTypes.STRING
                "required": true
            ]
            type: Constants.ApplicationCommandTypes.CHAT_INPUT
        bot.createCommand
            name: "queue"
            description: "View the queue."
            type: Constants.ApplicationCommandTypes.CHAT_INPUT
        bot.createCommand
            name: "stop"
            description: "Stop playing or skip to the next song."
            type: Constants.ApplicationCommandTypes.CHAT_INPUT

bot.on "error", (err) ->
    console.error err

queue = []
stopSwitch = false

playYt = (interaction, connection) ->
    link = queue.pop()
    metadata = await youtube.getVideoInfo link
    filename = metadata.id + ".opus"
    interaction.createMessage "Now playing: " + metadata.title
    youtubeEvent = youtube.exec [link, "-x", "--audio-format", "opus", "--no-continue", "--no-part", "-o", filename]
        .on "progress", (progress) ->
            console.log progress.percent
        .on "youtubeDlEvent", (eventType, eventData) ->
            console.log eventType, eventData
        .on "error", (err) ->
            console.error err
            interaction.createFollowup "ERROR\n" + err.message
        .on "closed", () ->
            connection.play filename
            connection.on "end", () ->
                fs.unlink filename, (err) ->
                    console.error err if err
                unless queue.length is 0
                    playYt interaction, connection

bot.on "interactionCreate", (interaction) ->
    if interaction instanceof Eris.CommandInteraction
        switch interaction.data.name
            when "play"
                unless interaction.member.voiceState.channelID is null
                    queue.push interaction.data.options[0].value
                    interaction.createMessage "Added to the queue."
                    bot.joinVoiceChannel interaction.member.voiceState.channelID
                        .then (connection) ->
                            playYt interaction, connection unless connection.playing
                        .catch (err) ->
                            interaction.createMessage "ERROR\n" + err.message
                            console.error err
                else interaction.createMessage "You are not in a voice channel."
            when "queue" then interaction.createMessage "Play queue:\n" + queue
            else
                interaction.createMessage "Command received."



bot.connect()
